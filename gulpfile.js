var gulp = require('gulp');

var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var postcss      = require('gulp-postcss');


var STYLES_PATH  = "./src/sass/**/*.scss";


gulp.task('sass', function () {
  return gulp.src(STYLES_PATH)
  	.pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./src/css'))
});


gulp.task('default', function(){
	gulp.watch([STYLES_PATH], ['sass']);
});