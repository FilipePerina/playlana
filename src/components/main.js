import "../css/main.css"
import React from "react"
import ReactDOM from "react-dom"
import TodoStore from "../stores/LanaStore"
import TodoList from "./TodoList"
import Canvas from './canvas/Canvas'

const app = document.getElementById("app")

ReactDOM.render(<Canvas store={TodoStore} />, app)

