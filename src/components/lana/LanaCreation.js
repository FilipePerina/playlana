import React from "react"
import {observer} from "mobx-react"


@observer
export default class LanaCreation extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            descriptionDisabled: "disabled"
        }
    }

    componentDidMount() {
        let _this = this;

        if(this.props.store.selectedLana.description || this.props.store.selectedLana.title) {
            this.setState({
                descriptionDisabled: ""
            });
        }
    }

    componentWillUpdate() {

    }

    handleTitle(e) {
        this.props.store.selectedLana.title = e.target.value;
    }

    handleDescription(e) {
        this.props.store.selectedLana.description = e.target.value;
    }

    openRemainingTitle() {
        $('.char-remaining.title, .title-input-holder button').css({
            display: 'block'
        });

        TweenLite.to( $('.lana-creation'), .5,
            {
                transform: 'translateX(-23px)',
                ease:Power2.easeOut
            }
        );


    }

    openRemainingDescription() {
        $('.char-remaining.description, .description-input-holder button').css({
            display: 'block'
        });

        TweenLite.to( $('.lana-creation'), .5,
            {
                transform: 'translateX(-23px)',
                ease:Power2.easeOut
            }
        );
    }

    closeRemaining() {
        $('.char-remaining.title, .char-remaining.description, .title-input-holder button, .description-input-holder button').css({
            display: 'none'
        });

        TweenLite.to( $('.lana-creation'), .5,
            {
                transform: 'translateX(0)',
                ease:Power2.easeOut
            }
        );
    }

    okTitle() {

    }

    render() {
        let _this = this
        var selectedLana = this.props.store.selectedLana;

        var maxTitle = 36;
        var maxDescription = 160;

        var titleColorClass = "";
        var descriptionColorClass = "";

        if(maxTitle - selectedLana.title.length < maxTitle){
            titleColorClass = "green";
        }
        if(maxTitle - selectedLana.title.length <= 11){
            titleColorClass = "yellow";
        }
        if(maxTitle - selectedLana.title.length <= 0){
            titleColorClass = "red";
        }

        if(maxDescription - selectedLana.description.length < maxDescription){
            descriptionColorClass = "green";
        }
        if(maxDescription - selectedLana.description.length <= 86){
            descriptionColorClass = "yellow";
        }
        if(maxDescription - selectedLana.description.length <= 0){
            descriptionColorClass = "red";
        }

        return (
            <div className="lana-creation-holder">

                <div className="lana-creation-overlay">
                    <button className="close-button black" onTouchEnd={this.props.store.closeLana.bind(this, this.props.centerCanvas, selectedLana.key )}></button>
                    <button className="delete-button black"></button>
                </div>

                <div className="lana-creation">
                    <p className="title-input-holder">
                        <input id="test" className="title-input"
                            value={selectedLana.title}
                            onChange={this.handleTitle.bind(this)}
                            onFocus={this.openRemainingTitle}
                            onBlur={this.closeRemaining}
                            autoCorrect="off"
                            autoCapitalize="on"
                            tabIndex="-1"
                            />

                        <button onTouchEnd={this.okTitle} className="ok-button"></button>
                    </p>
                    <p className="description-input-holder">
                        <textarea
                            className="description-input"
                            value={ selectedLana.description }
                            onChange={this.handleDescription.bind(this)}
                            onFocus={this.openRemainingDescription}
                            onBlur={this.closeRemaining}
                            tabIndex="-1"
                            disabled={ this.state.descriptionDisabled }
                            ></textarea>
                    </p>

                    <ul className="lana-icons yellow">
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>

                <div className={"char-remaining title " + titleColorClass}><strong>{ maxTitle - selectedLana.title.length }</strong> Characters Remaining</div>
                <div className={"char-remaining description " + descriptionColorClass}><strong>{ maxDescription - selectedLana.description.length }</strong> Characters Remaining</div>

            </div>


        )
    }
}

var lanacreation = window.lanacreation = new LanaCreation
