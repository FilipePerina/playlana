import React from "react"
import {observer} from "mobx-react"
import Sortable from 'sortablejs'
import DevTools from 'mobx-react-devtools';

import LanaCreation from '../lana/LanaCreation';

Array.min = function(array) {
    return Math.min.apply(Math, array);
};

var draggableCanvas;
let oldId = 0, newId = 0;
var isDragging = false;
var isLanaDragging = false;

var LanaCreationOpen = false;

var minTop, minLeft;

@observer
export default class Canvas extends React.Component {
    constructor(props) {
        super(props);

        // this.state = {
        //     isDragging: false,
        //     oldId: 0,
        //     newId: 0
        // }
    }

    componentDidMount() {
        let _this = this;

        this.props.store.loadLanas();

        draggableCanvas = Draggable.create('#canvas', {
            type: 'scroll',
            cursor: 'default',
            edgeResistance: .9,
            throwProps: true,
            onDragStart: () => {
                isDragging = true;
                // _this.setState({ isDragging: true });
                $('#canvas > div:nth-child(2)').css({ 'display': 'none' });

                return true;
            },
            onDragEnd: () => {
                setTimeout(() => {
                    isDragging = false;
                    // _this.setState({ isDragging: false });
                }, 100);

                return true;
            }
        });

        setTimeout(function(){
            _this.centerCanvas(0, 0);
        }, 500);

        setTimeout(function(){
            $('#canvas').addClass('active');
        }, 600);

    }

    centerCanvas(obj = 0, speed = 1, delay = 0) {
        var $element = $('.lana_spot[data-index="'+ obj +'"]');
        var offset = $element.position();

        var wW = (( $(window).width() / 2  ) - 113) / 2;
        var wH = (( $(window).height() / 2  ) - 165) / 2;

        var marginW = 0;
        var marginH = 50;

        if( $(window).width() > 320 ){
            marginW = 10;
        }

        if( $(window).width() >= 414 ){
            marginW = 20;
        }

        if( $(window).height() > 568 ){
            marginH = 85;
        }


        // console.log(offset.left, minTop, $(window).width() );
        var objX = offset.left + (minLeft - wW - marginW);
        var objY = offset.top + (minTop - wH - marginH);


        TweenLite.to( $('#canvas'), speed,
            {
                scrollTo: { x : objX, y : objY },
                delay: delay,
                ease:Power2.easeOut
            }
        );

    }

    checkHolderSize() {
        var elements = {
            top: [],
            left: []
        };

        var horSize = 0;
        var verSize = 0;

        var $lanaHolder = $('.lanas_holder');

        $('.lanas_holder .lana_spot').each(function(index, value) {
            var lana_pos = $(this).position();

            if (lana_pos.top < 0) {
                elements.top.push(lana_pos.top);
            }

            if (lana_pos.left < 0) {
                elements.left.push(lana_pos.left);
            }

            horSize = lana_pos.left;
            verSize = lana_pos.top;

        });

        minTop = Array.min(elements.top) * -1;
        minLeft = Array.min(elements.left) * -1;

        $lanaHolder.css({
            'transform': "translate(" + minLeft + "px, " + minTop + "px)",
            'width': horSize + 190 + "px",
            'height': verSize + 210 + 40 + "px"
        });

    }

    selectLana(index, e) {
        var id = index;
        $('.lana_spot[data-index="'+ id +'"] .lana').addClass('active');
    }

    addLana(index, e) {
        draggableCanvas[0].enable();

        if(isDragging){ return false; }

        var id = Number(e.target.getAttribute('data-index'));
        var x = Number(e.target.getAttribute('data-coordx'));
        var y = Number(e.target.getAttribute('data-coordy'));

        $('.lana_spot[data-index="'+ index +'"] .lana').removeClass('active');

        this.props.store.selectLana(index, this.props.store, this.centerCanvas);

        this.checkHolderSize();

    }

    removeSelector(e) {
        setTimeout(function() {
            e.target.className = "lana_spot";
        }, 1000);
    }

    clickLana(lana, index) {
        if(!isLanaDragging){
            this.props.store.editLana(lana, index);
        }

    }

    renderLana(lana, index) {
        if(lana.status === true) {
            return (
                <div className="lana" onTouchEnd={this.clickLana.bind(this, lana, index)}>
                    <div className="lana-thumb">
                        <p className="lana-title">{ lana.lana.title }</p>
                        <img src="images/lana-text.png" />
                        <ul className="lana-icons yellow">
                            <li></li>
                            <li></li>
                            <li></li>
                        </ul>
                    </div>
                </div>
            )
        } else {
            return ""
        }
    }

    initializeSortable() {
        let _this = this;

        var $lana = null;


        $(".sortable_spot").droppable({
            over: function (event, ui) {
               newId = $(this).data('index');

               var lanaStatus = _this.props.store.lanas[newId].status;

               $('.lana_spot').removeClass('blocked');

                // console.log(newId);

               if( lanaStatus === true && oldId != newId){
                   $('.lana_spot[data-index="'+ newId +'"]').addClass('blocked');
               }
           }
        });

        $(".lana_spot .lana").draggable({
            cancel: 'input,textarea,button,select,option',
            connectToSortable: ".sortable_spot",
            scroll: false,
            revert: true,
            revertDuration: 0,
            start: function(event, ui) {
                // let theOldId = $(ui.helper[0]).parent().parent().data('id');
                if(LanaCreationOpen){ return false; }

                isLanaDragging = true;

                $lana = ui.helper;
                $('.lana').addClass('disabled');
                $lana.addClass('active');

                let theOldId = $(ui.helper).parent().data('index');

                if(theOldId === undefined){
                    theOldId = 1;
                }

                oldId = theOldId;

                $(this).data("startingScrollTop", $(this).offsetParent().scrollTop());
            },

            drag: function(event, ui) {
                if(LanaCreationOpen){ return false; }

                isLanaDragging = true;

                var st = parseInt($(this).data("startingScrollTop"));
                ui.position.top -= $(this).offsetParent().scrollTop() - st;
            },
            stop: function(event, ui) {
                draggableCanvas[0].enable();
                setTimeout(function(){
                    isLanaDragging = false;
                }, 100);

                $('.lana').removeClass('disabled');
                // $lana.removeClass('active');
                $(ui.helper).removeClass('active');
                // $('.lana_spot').sortable('cancel');

                $('.lana_spot').removeClass('blocked');


                _this.props.store.moveLana( oldId, newId, store );

                if(oldId !== newId){
                    _this.props.store.removeLanaSpace(oldId, oldId, newId);
                    _this.props.store.moveLana( oldId, newId, store );
                }
            }
        });

        $('.lana_spot .lana').on('touchmove', function(e) {
            draggableCanvas[0].disable();
        });

        $('.lana_spot .lana').on("touchstart", function(e) {
            draggableCanvas[0].disable();
        });

        function cancelSortable() {
            $('.sortable_spot').sortable('cancel');
        }

    }

    moveLana(oldI, newI, store) {
        var _this = this;
        setTimeout(function() {
            store.moveLana( 0, 2, store );
        }, 100);
    }

    render() {
        // console.log('Render');
        let _this = this
        setTimeout(function() {
            _this.checkHolderSize();
            _this.initializeSortable();
        }, 200)

        let lanas = this.props.store.lanas

        let offsetX = this.props.store.offsetX
        let offsetY = this.props.store.offsetY

        let i = 0;

        // console.log( this.props.store.lanas.slice() )

        const lanasList = lanas.values().map((lana, index) => {
            let sortableClass = " sortable_spot ";
            let title = "";
            if(lana.status === true){
                sortableClass = "";
            }

            if(lana.status === true){
                title = lana.lana['title'];
            }

            return (
                <div
                    key={index}
                    data-id={lana.id}
                    data-coord={lana.coord[0] + '' + lana.coord[1]}
                    data-coordx={lana.coord[0]}
                    data-coordy={lana.coord[1]}
                    onTouchStart={this.selectLana.bind(this, index)}
                    onTouchEnd={this.addLana.bind(this, index)}
                    className={" lana_spot " + sortableClass + lana.selectedClass}
                    data-index={index}
                    data-status={lana.status}
                    data-native={lana.native}
                    style={{left: (lana.coord[0] * offsetX), top: (lana.coord[1] * offsetY)}} >
                    {this.renderLana(lana, index)}
                </div>
            )
        })

        return (
            <div className="main">
                <div className="canvas" id="canvas">
                    <div className="lanas_holder">
                        {lanasList}
                    </div>
                </div>

                <LanaCreation store={this.props.store} centerCanvas={this.centerCanvas } />

                <div className="version-holder">
                    { "0.2.0" }
                </div>
            </div>

        )
    }
}

var canvas = window.canvas = new Canvas
