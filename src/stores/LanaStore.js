import { computed, observable, autorun, action, extendObservable, asMap } from "mobx"
import axios from 'axios'
import DefaultLanas from './DefaultLanas'
import Canvas from '../components/canvas/Canvas'


export class TodoStore {
    @observable lanas = asMap([])

    @observable offsetX = 190
    @observable offsetY = 245

    @observable states = {}

    @observable selectedLana = {
        title: "",
        description: ""
    }
    @observable selectedIndex = 0


    loadLanas() {
        let _this = this;

        this.lanas.merge(DefaultLanas)

        // console.log( this.lanas.get[0] );



    }

    @action selectLana(id, store, centerCanvas) {
        var _this = this;
        var lanas = this.lanas;
        var elementPos = id;

        // console.log(lanas.get(elementPos).selectedClass);

        var lana = lanas.get(elementPos);

        if(lana.selectedClass != "active"){

            let update = {...lana, selectedClass: "active" };

            // lanas[elementPos] = observable(update);
            lanas.get(elementPos).selectedClass = "active";

            setTimeout(function(){
                lanas.get(elementPos).selectedClass = "";
            }, 1250);
        }else{

            this.editLana(store.lanas[elementPos], elementPos);

            lanas.get(elementPos).status = true;

            this.addLanaSpace(lana.coord[0], lana.coord[1]);

        }


    }

    spaceHasLana(id) {
        var lanas = this.lanas;
        var elementPos = lanas.map(function(x) {return x.id; }).indexOf(id);
        //console.log(id);

        if(elementPos < 0){ return false; }

        if(lanas[elementPos].lana !== undefined){
            return true;
        }else{
            return false;
        }
    }

    @action moveLana(oldId, newId, store) {

        // console.log(newId, oldId);

        var thisNewLana = this.lanas.get(newId);
        var thisOldLana = this.lanas.get(oldId);

        this.addLanaSpace( thisNewLana.coord[0], thisNewLana.coord[1]  );

        // var newLanas = this.lanas.values();

        if(thisNewLana.status === true){ return false };

        // if( newLanas[newId].status === true ){ return false; }

        thisOldLana.status = false;
        thisNewLana.status = true;
        thisNewLana.lana = thisOldLana.lana;

        // 
        // newLanas[newId].lana = newLanas[oldId].lana;
        //
        // this.lanas = observable(newLanas);
        //


        return false;

    }

    addLanaSpace(x, y) {
        var lanas = this.lanas;

        var oldLanas = this.lanas.values();
        var newLanas = [];

        for(var i = -1; i <= 1; i++){
            var s1 = lanas.values().filter(function (lana) { return lana.coord[0] == (x + i) && lana.coord[1] == (y - 1) })
            var s2 = lanas.values().filter(function (lana) { return lana.coord[0] == (x + i) && lana.coord[1] == (y + 0) })
            var s3 = lanas.values().filter(function (lana) { return lana.coord[0] == (x + i) && lana.coord[1] == (y + 1) })

            var key1 = "";
            var key2 = "";
            var key3 = "";

            if(!s1.length){
                key1 = Date.now() * (i + 2);

                var obj = {
                    coord: [(x+i) , (y-1)],
                    id: key1,
                    selectedClass: "",
                    status: false,
                    native: false,
                    lana: {
                        title: "",
                        description: ""
                    }
                }

                newLanas.push(obj);

            }

            if(!s2.length){
                key2 = Date.now() * (i + 3);

                var obj = {
                    coord: [(x+i) , (y+0)],
                    id: key2,
                    selectedClass: "",
                    status: false,
                    native: false,
                    lana: {
                        title: "",
                        description: ""
                    }
                }

                newLanas.push(obj);
                // lanas.values().push(obj);
            }

            if(!s3.length){
                key3 = Date.now() * (i + 4);

                var obj = {
                    coord: [(x+i) , (y+1)],
                    id: key3,
                    selectedClass: "",
                    status: false,
                    native: false,
                    lana: {
                        title: "",
                        description: ""
                    }
                }

                newLanas.push(obj);
                // lanas.values().push(obj);

            }


        }

        var updatedLanas = oldLanas.concat(newLanas);

        this.lanas.merge(updatedLanas);
    }

    removeLanaSpace(index, oldId, newId) {
        var _this = this;
        var lanas = this.lanas;

        var newLanas = lanas.slice();

        var lanasArray = [];

        var x = $('.lanas_holder .lana_spot[data-index="'+ index +'"]').data('coordx');
        var y = $('.lanas_holder .lana_spot[data-index="'+ index +'"]').data('coordy');

        var isDeletable = true;

        for(var i = -1; i <= 1; i++){
            var s1 = lanas.filter(function (lana) { return lana.coord[0] == (x + i) && lana.coord[1] == (y - 1) })
            var s2 = lanas.filter(function (lana) { return lana.coord[0] == (x + i) && lana.coord[1] == (y + 0) })
            var s3 = lanas.filter(function (lana) { return lana.coord[0] == (x + i) && lana.coord[1] == (y + 1) })

            // console.info(i);

            var $s1 = $('.lanas_holder .lana_spot[data-coord="'+ (x + i) + "" + (y - 1) +'"]');
            var $s2 = $('.lanas_holder .lana_spot[data-coord="'+ (x + i) + "" + (y + 0) +'"]');
            var $s3 = $('.lanas_holder .lana_spot[data-coord="'+ (x + i) + "" + (y + 1) +'"]');

            if(!$s1.data('native')){
                // console.warn( $s1.find('.lana').length, $s1.data('index')  );
                if($s1.find('.lana').length === 0 ){
                    // console.log('deletável', $s1.data('index'));

                    lanasArray.push( $s1.data('index') );
                }else{
                    // console.log('não deletável');
                    // console.warn( $s1.data('index'), newId  );
                    isDeletable = false;
                }
            }
            if(!$s2.data('native')){
                // console.warn( $s2.find('.lana').length, $s2.data('index')  );
                if($s2.find('.lana').length === 0 ){
                    // console.log('deletável', $s2.data('index') );

                    lanasArray.push( $s2.data('index') );
                }else{
                    // console.log('não deletável');
                    // console.warn( $s2.data('index'), newId  );
                    isDeletable = false;
                }
            }
            if(!$s3.data('native')){
                // console.warn( $s3.find('.lana').length, $s3.data('index')  );
                if($s3.find('.lana').length === 0 ){
                    // console.log('deletável', $s3.data('index') );

                    lanasArray.push( $s3.data('index') );
                }else{
                    // console.log('não deletável');
                    // console.warn( $s3.data('index'), newId  );
                    isDeletable = false;
                }
            }

            if( $s2.data('index') !== newId ){
                isDeletable = true;
            }

        }

        // console.log(lanasArray);
        // console.log(isDeletable);

        if(isDeletable){
            lanasArray.map((lanaIndex) => {
                //if(lanaIndex != index){
                    delete newLanas[lanaIndex];
                //}

                // $('.lana_spot[data-index="'+ lanaIndex +'"]').remove();
            });

            this.lanas = observable(newLanas);
        }

        $('.lana_spot').each(function(index){
            if ($(this).find('.lana').length > 0 ){
                var id = $(this).data('index');
                //console.log(id);
                _this.moveLana(id, id);
            }
        });

    }


    openLana(index) {
        var canvas_diff = $('.lanas_holder').offset();
        var windowWidth = $(window).width() - 50;
        var windowHeight = $(window).height() - 89 - 74;

        TweenLite.to( $('.lana_spot[data-index="'+ index +'"] .lana'), 1,
            {
                transform: 'translate(-' + (canvas_diff.left - 25) +'px, -'+ (canvas_diff.top - 75) +'px)',
                width: windowWidth,
                height: windowHeight,
                zIndex: 15,
                ease:Power2.easeOut
            }
        );

        TweenLite.to( $('.lana_spot[data-index="'+ index +'"] .lana-thumb'), .5,
            {
                opacity: 0,
                pointerEvents: 'none',
                ease:Power2.easeOut,
            }
        );

        TweenLite.to( $('.lana_spot[data-index="'+ index +'"] .lana-creation'), .5,
            {
                opacity: 1,
                pointerEvents: 'all',
                ease:Power2.easeOut,
            }
        );
    }

    editLana(lana, index, centerLana){
        var lanas = this.lanas;

        //console.log(index);
        store.selectedIndex = index;
        this.selectedLana = lanas.get(index).lana;

        TweenLite.to( $('.lana-creation-holder .lana-creation'), 0, { transform: 'scale(0)', opacity: 0 } );

        TweenLite.to( $('.lana-creation-holder'), 1, {
                opacity: 1,
                pointerEvents: 'all',
                ease:Power2.easeOut
            }
        );

        TweenLite.to( $('.lana-creation-holder .lana-creation'), 1.3, {
                opacity: 1,
                transform: 'scale(1)',
                ease:Power2.easeOut
            }
        );
    }

    closeLana(centerCanvas, index){
        TweenLite.to( $('.lana-creation-holder'), 1.3, {
                opacity: 0,
                pointerEvents: 'none',
                ease:Power2.easeOut
            }
        );

        TweenLite.to( $('.lana-creation-holder .lana-creation'), 1, {
                opacity: 0,
                transform: 'scale(0)',
                ease:Power2.easeOut
            }
        );



        centerCanvas(store.selectedIndex, .25);
    }

    createLana(index) {
        this.openLana(index);
    }


}

var store = window.store = new TodoStore()

export default new TodoStore
