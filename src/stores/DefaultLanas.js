var lanas = [
	{
		"id": 1,
		"coord": [0,0],
		"lana": {
			"title": "Solar system",
			"description": "The Solar System is the gravitationally bound system comprising the Sun and the objects that orbit the Sun directly."
		},
        "status": true,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 2,
		"coord": [-1,0],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 3,
		"coord": [-1,-1],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 4,
		"coord": [-1,1],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 5,
		"coord": [0,1],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 6,
		"coord": [1,1],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 7,
		"coord": [1,0],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 8,
		"coord": [1,-1],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	},
	{
		"id": 9,
		"coord": [0,-1],
		"lana": {
			"title": "",
			"description": ""
		},
        "status": false,
		"selectedClass": "",
		"native": true
	}
]

export default lanas
